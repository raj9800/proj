package com.nswbc.ba.core.models.gatedarticle;

import javax.inject.Inject;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

/**
 * GatedArticleModel Class.
 *
 * @author rajat.pachouri.
 *
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-ba/components/content/gated-article",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class GatedArticleModel {

    /**
     * private inject variable for promoBgColor.
     */
    @Inject
    @Via("resource")
    @Default(values = "g-bg-color-snow")
    private String bgColor;
    /**
     * private inject variable for promoBgColor.
     */
    @Inject
    @Via("resource")
    private String bgColorRGB;

    /**
     * private inject variable for isGated .
     */
    @Inject
    @Via("resource")
    private String isGated;
    /**
     * private inject variable for HeadingType.
     */
    @Inject
    @Via("resource")
    @Default(values = "image")
    private String headingType;

    /**
     * private inject variable for Button text.
     */
    @Inject
    @Via("resource")
    private String contentType;
    /**
     * private inject variable for Button text.
     */
    @Inject
    @Via("resource")
    private String docURL;
    /**
     * private inject variable for image.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String fileReference;
    /**
     * private inject variable for Bg image.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String fileReferenceBg;
    /**
     * private inject variable for promoHeading.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String heading;

    /**
     * private inject variable for promoSubHeading.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String subHeading;

    /**
     * private inject variable for promoSubHeading.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String successSubHeading;

    /**
     * private inject variable for subHeadingFontColor.
     */
    @Inject
    @Via("resource")
    private String textColor;

    /**
     * private inject variable for button type.
     */
    @Inject
    @Via("resource")
    private String linkType;

    /**
     * private inject variable for Button text.
     */
    @Inject
    @Via("resource")
    private String linkLabel;

    /**
     * private inject variable for subHeadingStrong.
     */
    @Inject
    @Via("resource")
    @Default(values = "false")
    private String subHeadingStrong;

    /**
     * private inject variable for imageType.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String emailHeading;

    /**
     * private inject variable for optin text.
     */
    @Inject
    @Via("resource")
    @Default(values = "")
    private String optinText;

    /**
     * @return the optinText
     */
    public String getOptinText() {
        return optinText;
    }

    /**
     * @return subtitle heading in strong.
     */
    public String getSubHeadingStrong() {
        return subHeadingStrong;
    }

    /**
     * @return promo heading font color.
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * @return promo background color.
     */
    public String getBgColor() {
        return bgColor;
    }

    /**
     * @return promo background color.
     */
    public String getBgColorRGB() {
        return bgColorRGB;
    }

    /**
     * @return promo heading.
     */
    public String getHeading() {
        return heading;
    }

    /**
     * @return promo subtitle heading.
     */
    public String getSubHeading() {
        return subHeading;
    }

    /**
     * @return heading type.
     */
    public String getHeadingType() {
        return headingType;
    }

    /**
     * getter for is gated.
     *
     * @return true if content is gated
     */
    public String getIsGated() {
        return isGated;
    }

    /**
     * @return button Type.
     */
    public String getLinkType() {
        return linkType;
    }

    /**
     * @return button label.
     */
    public String getLinkLabel() {
        return linkLabel;
    }

    /**
     * @return image URL.
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * @return image URL.
     */
    public String getFileReferenceBg() {
        return fileReferenceBg;
    }

    /**
     * Getter for content type.
     *
     * @return article or download
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * getter for url of document.
     *
     * @return modified URL of document to allow forced download.
     */
    public String getDocURL() {
        StringBuilder downloadUrlBuilder = new StringBuilder();
        if (StringUtils.isNotBlank(docURL)) {
            String ext = FilenameUtils.getExtension(docURL);
            if (StringUtils.isNotBlank(ext)) {
                downloadUrlBuilder.append(docURL).append(".").append("coredownload").append(".").append(ext);
            }
        }
        return downloadUrlBuilder.toString();
    }

    /**
     * Getter for success subheading.
     *
     * @return success sub heading.
     */
    public String getSuccessSubHeading() {
        return successSubHeading;
    }

    /**
     * @return image Type
     */
    public String getEmailHeading() {
        return emailHeading;
    }
}
