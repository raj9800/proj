package com.nswbc.ba.core.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.ActiveTokenCheckService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = ActiveTokenCheckService.class)
public class ActiveTokenCheckServiceImpl implements ActiveTokenCheckService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(ActiveTokenCheckServiceImpl.class);
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /**
     * private reference for registration configuration.
     */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;

    /**
     * This method checks the abn from crm api and returns the status response.
     */
    @Override
    public JsonObject verifyToken(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String token = request.getParameter("token");
        if (Objects.isNull(token) && StringUtils.isEmpty(token)) {
            return RestUtil.getBadRequestResponse(response);
        }
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty(Constants.Application.EMPTY, Constants.Application.EMPTY);
        Map<String, String> headerMap = setHeaderMap(token);
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpResponse restResponse = RestUtil.processRestPostRequest(client, registerConfig.getConfig().tokenVerificationAPI(), headerMap,
                requestBody);
        JsonObject resultStatus = new JsonObject();
        if (Objects.isNull(restResponse) || restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
            LOG.error("IDAM server error");
            return RestUtil.getServerErrorResponse(response);
        } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
            resultStatus.addProperty("status", Constants.Number.ONE);
            return resultStatus;
        } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED_ONE) {
            resultStatus.addProperty("status", Constants.Number.ZERO);
            return resultStatus;
        }
        return RestUtil.getServerErrorResponse(response);
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeaderMap(String token) {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, registerConfig.getConfig().subscriptionKey());
        headerMap.put("Authorization", "bearer " + token);
        return headerMap;
    }

}
