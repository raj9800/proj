package com.nswbc.ba.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.AbnGovLookupService;
import com.nswbc.ba.core.services.RegistrationService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = RegistrationService.class)
public class RegistrationServiceImpl implements RegistrationService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(RegistrationServiceImpl.class);
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /**
     * private reference for registration configuration.
     */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;

    /** private reference variable for abn gov verification. */
    @Reference
    private AbnGovLookupService abnGovVerify;

    /**
     * This method returns the response json object in registration call.
     */
    @Override
    public JsonObject register(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        JsonObject bodyJson = RestUtil.getBodyJson(request);
        boolean validate = (Objects.nonNull(bodyJson) ? verifyInputFields(bodyJson) : false);
        if (validate) {
            Map<String, String> headerMap = setHeadersMap();
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse restResponse = RestUtil.processRestPostRequest(client, registerConfig.getConfig().registrationAPI(), headerMap,
                    bodyJson);
            if (Objects.nonNull(restResponse) && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getServerErrorResponse(response);
                }
                try {
                    String responseEntity = EntityUtils.toString(restResponse.getEntity());
                    JsonObject registrationAPIResult = new JsonParser().parse(responseEntity).getAsJsonObject();
                    if (registrationAPIResult.get(STATUS).getAsBoolean() == Boolean.TRUE) {
                        JsonObject registrationFinalResult = new JsonObject();
                        registrationFinalResult.addProperty(STATUS, Constants.Number.ONE);
                        return registrationFinalResult;
                    } else {
                        RestUtil.getServerErrorResponse(response);
                    }
                } catch (ParseException | IOException e) {
                    LOG.error("Unable to register : ", e.getMessage());
                    return RestUtil.getServerErrorResponse(response);
                } finally {
                    RestUtil.closeHttpClient(client);
                }

            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @param bodyJson This method is use to verify input form fields
     * @return validation status for fields
     */
    private boolean verifyInputFields(JsonObject bodyJson) {
        boolean validate = false;
        boolean validateFirstName = false;
        boolean validateLastName = false;
        boolean validateEmail = false;
        String firstName = bodyJson.get("firstname").toString();
        String lastName = bodyJson.get("lastname").toString();
        String email = bodyJson.get("email").toString();
        if (Objects.nonNull(firstName) && StringUtils.isNotBlank(firstName)) {
            validateFirstName = true;
        }
        if (Objects.nonNull(lastName) && StringUtils.isNotBlank(lastName)) {
            validateLastName = true;
        }
        if (Objects.nonNull(email) && StringUtils.isNotBlank(email)) {
            validateEmail = true;
        }
        if (validateFirstName && validateLastName && validateEmail) {
            validate = true;
            return validate;
        }
        return validate;
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, registerConfig.getConfig().subscriptionKey());
        return headerMap;
    }
}
