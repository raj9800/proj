package com.nswbc.ba.core.services;

import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface AbnCrmLookupService {
    /**
     * @param abn
     * @return abn check
     */
    JsonObject verifyABN(String abn, SlingHttpServletResponse response);
}
