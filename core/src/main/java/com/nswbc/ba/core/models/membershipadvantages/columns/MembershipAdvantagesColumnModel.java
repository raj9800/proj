package com.nswbc.ba.core.models.membershipadvantages.columns;

import java.util.Objects;

import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;

import com.day.cq.dam.api.Asset;

/**
 * Membership advantages column model.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class},
        resourceType = "nswbc-commons/components/content/button",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class MembershipAdvantagesColumnModel {

    /** Constant for dc:title. */
    private static final String DC_TITLE = "dc:title";

    /** private sling object for resource resolver. */
    @Inject
    private ResourceResolver resourceResolver;
    /**
     * private inject variable for fileReference.
     */
    @Inject
    @Default(values = "")
    private String title;
    /**
     * private inject variable for fileReference.
     */
    @Inject
    @Default(values = "")
    private String fileReference;

    /**
     * private inject variable for imgAltText.
     */
    @Inject
    @Default(values = "")
    private String imgAltText;

    /**
     * @return fileReference
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Getter method for image alt text.
     *
     * @return alt text given via author, if not given, returns title set in the dam
     *         for image
     */
    public String getImgAltText() {
        String title = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(imgAltText)) {
            return imgAltText;
        } else {
            if (Objects.nonNull(fileReference)) {
                Resource resource = resourceResolver.getResource(fileReference);
                Asset asset = resource.adaptTo(Asset.class);
                title = asset.getMetadataValue(DC_TITLE);
            }
            return title;
        }
    }
}
