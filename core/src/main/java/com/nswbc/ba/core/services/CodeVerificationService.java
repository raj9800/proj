package com.nswbc.ba.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface CodeVerificationService {
    /**
     * @param request
     * @return code verification status
     */
    JsonObject verifySecurityCode(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
