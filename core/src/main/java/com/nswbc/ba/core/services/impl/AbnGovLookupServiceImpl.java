package com.nswbc.ba.core.services.impl;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.AbnCrmLookupService;
import com.nswbc.ba.core.services.AbnGovLookupService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;
import com.nswbc.commons.core.utils.ServerSideValidationUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = AbnGovLookupService.class)
public class AbnGovLookupServiceImpl implements AbnGovLookupService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(AbnGovLookupServiceImpl.class);
    /** Constant for abn. */
    private static final String ABN = "abn";
    /** Constant for company name. */
    private static final String COMPANY_NAME = "companyname";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant of regex to extract json output from response. */
    private static final String ABN_OUTPUT_REGEX = "(\\{.*?\\})";
    /**
     * private reference for registration configuration.
     */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;
    /** private reference variable for abn crm verification. */
    @Reference
    private AbnCrmLookupService abnCrmVerify;

    /**
     * This method checks the mail from api and returns the status response.
     */
    @Override
    public JsonObject verifyABNGov(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String abn = request.getParameter(ABN);
        boolean validate = verifyInputField(abn);
        if (validate) {
            StringBuilder builder = buildQueryParam(abn);
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse restResponse = RestUtil.processRestGetRequest(client, registerConfig.getConfig().abnGovAPI(), builder, null);
            if (Objects.nonNull(restResponse)) {
                try {
                    return processGetResponse(abn, client, restResponse, response);
                } catch (ParseException | IOException e) {
                    LOG.error("Unable to get abn status : ", e.getMessage());
                    return RestUtil.getServerErrorResponse(response);
                }
            } else {
                return RestUtil.getServerErrorResponse(response);
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @param abn
     * @return validation on abn field
     */
    private boolean verifyInputField(String abn) {
        boolean validate = false;
        if (Objects.nonNull(abn) && StringUtils.isNotBlank(abn)) {
            validate = ServerSideValidationUtil.checkABNField(abn);
        }
        return validate;
    }

    /**
     * @param abn2
     * @throws IOException exception
     * @param client
     * @param restResponse
     * @param response
     * @return json response
     */
    private JsonObject processGetResponse(String abn, CloseableHttpClient client, HttpResponse restResponse, SlingHttpServletResponse response)
            throws IOException {
        JsonObject status = new JsonObject();
        String responseEntity = EntityUtils.toString(restResponse.getEntity());
        client.close();
        JsonObject abnResult = extractJsonData(responseEntity);
        if (!abnResult.isJsonNull()) {
            String companyName = abnResult.get("EntityName").getAsString();
            if (StringUtils.equals(companyName, Constants.Application.EMPTY)) {
                status.addProperty(STATUS, Constants.Number.ZERO);
            } else {
                JsonObject crmResponse = abnCrmVerify.verifyABN(abn, response);
                int crmStatus = crmResponse.get(STATUS).getAsInt();
                if (crmStatus == Constants.Number.ZERO) {
                    status.addProperty(STATUS, Constants.Number.ONE);
                    status.addProperty(COMPANY_NAME, companyName);
                } else if (crmStatus == Constants.Number.ONE) {
                    status.addProperty(STATUS, Constants.Number.TWO);
                    status.addProperty(COMPANY_NAME, companyName);
                } else if (crmStatus == Constants.Number.FOUR_HUNDRED) {
                    status = RestUtil.getBadRequestResponse(response);
                } else if (crmStatus == Constants.Number.FIVE_HUNDRED) {
                    status = RestUtil.getServerErrorResponse(response);
                }
            }

        }
        return status;
    }

    /**
     * @param responseEntity
     * @return json object of result
     */
    private JsonObject extractJsonData(String responseEntity) {
        JsonObject abnResult = new JsonObject();
        Pattern pattern = Pattern.compile(ABN_OUTPUT_REGEX);
        Matcher matcher = pattern.matcher(responseEntity);
        if (matcher.find()) {
            abnResult = new JsonParser().parse(matcher.group(1)).getAsJsonObject();
        }
        return abnResult;

    }

    /**
     * @param abn
     * @return builder
     */
    private StringBuilder buildQueryParam(String abn) {
        StringBuilder builder = new StringBuilder();
        builder.append(Constants.Application.QUESTION_MARK)
        .append(ABN + Constants.Application.EQUAL + abn)
        .append(Constants.Application.AMPERSAND)
        .append("guid" + Constants.Application.EQUAL + registerConfig.getConfig().abnGovAPIGUID());
        return builder;
    }

}
