package com.nswbc.ba.core.configuration.registration;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.metatype.annotations.Designate;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = RegistrationAPIServiceConfigurationInstance.class, configurationPolicy = ConfigurationPolicy.REQUIRE)
@Designate(ocd = RegistrationAPIServiceConfiguration.class)
public class RegistrationAPIServiceConfigurationInstance {
    /** private configuration variable. */
    private RegistrationAPIServiceConfiguration config;

    /**
     * @param config
     */
    @Activate
    public void activate(RegistrationAPIServiceConfiguration config) {
        this.config = config;
    }

    /**
     * @return the config
     */
    public RegistrationAPIServiceConfiguration getConfig() {
        return config;
    }

}
