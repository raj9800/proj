package com.nswbc.ba.core.servlets;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.google.gson.JsonObject;
import com.nswbc.ba.core.services.AbnGovLookupService;
import com.nswbc.ba.core.services.ActiveTokenCheckService;
import com.nswbc.ba.core.services.AgentAssistedRegistrationService;
import com.nswbc.ba.core.services.CodeVerificationService;
import com.nswbc.ba.core.services.EmailVerificationService;
import com.nswbc.ba.core.services.RegistrationService;
import com.nswbc.commons.core.constants.Constants;

/**
 * @author yuvraj.bansal
 *
 */
@Component(service = Servlet.class, property = {"sling.servlet.selectors=" + UserRegistrationServlet.EMAIL_CHECK,
        "sling.servlet.selectors=" + UserRegistrationServlet.REGISTER,
        "sling.servlet.selectors=" + UserRegistrationServlet.AGENT_ASSISTED_REGISTER,
        "sling.servlet.selectors=" + UserRegistrationServlet.ABN_GOV_CHECK,
        "sling.servlet.selectors=" + UserRegistrationServlet.ACTIVATE,
        "sling.servlet.selectors=" + UserRegistrationServlet.SECURITY_CODE,
        "sling.servlet.paths=" + UserRegistrationServlet.SERVLET_PATH,
        "sling.servlet.extensions=" + Constants.Text.JSON, "sling.servlet.methods=GET", "sling.servlet.methods=POST"})
public class UserRegistrationServlet extends SlingAllMethodsServlet {

    /** generated serial version uid. */
    private static final long serialVersionUID = -2208591717522459699L;
    /** protected variable for servlet path. */
    protected static final String SERVLET_PATH = "/bin/nswbc/ba/register";
    /** protected selector variable for email check. */
    protected static final String EMAIL_CHECK = "emailcheck";
    /** protected selector variable for registration. */
    protected static final String REGISTER = "register";
    /** protected selector variable for agent assisted registration. */
    protected static final String AGENT_ASSISTED_REGISTER = "assistedregister";
    /** protected selector variable for abn check. */
    protected static final String ABN_GOV_CHECK = "abngovcheck";
    /** protected selector variable to check active registration. */
    protected static final String ACTIVATE = "activate";
    /** protected selector variable to verify security code. */
    protected static final String SECURITY_CODE = "codeverify";

    /** private reference variable for email verification. */
    @Reference
    private EmailVerificationService emailVerify;
    /** private reference variable for registration. */
    @Reference
    private RegistrationService registration;
    /** private reference variable for registration. */
    @Reference
    private AgentAssistedRegistrationService assistedRegistration;
    /** private reference variable for token verification. */
    @Reference
    private ActiveTokenCheckService activeTokenVerify;
    /** private reference variable for code verification. */
    @Reference
    private CodeVerificationService codeVerify;
    /** private reference variable for abn gov verification. */
    @Reference
    private AbnGovLookupService abnGovVerify;

    /**
     * doGet method to provide check between email and registration flow.
     */
    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(EMAIL_CHECK)) {
            JsonObject result = emailVerify.verifyEmail(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(ABN_GOV_CHECK)) {
            JsonObject result = abnGovVerify.verifyABNGov(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(ACTIVATE)) {
            JsonObject result = activeTokenVerify.verifyToken(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * doPost method to register.
     */
    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws IOException {
        response.setContentType(Constants.Application.APPLICATION_JSON);
        String selector = request.getRequestPathInfo().getSelectorString();
        if (selector.equals(REGISTER)) {
            JsonObject result = registration.register(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(AGENT_ASSISTED_REGISTER)) {
            JsonObject result = assistedRegistration.register(request, response);
            printResponseResult(response, result);
        } else if (selector.equals(SECURITY_CODE)) {
            JsonObject result = codeVerify.verifySecurityCode(request, response);
            printResponseResult(response, result);
        }
    }

    /**
     * @param response
     * @param result
     * @throws IOException This method checks the output result
     */
    private void printResponseResult(SlingHttpServletResponse response, JsonObject result) throws IOException {
        if (Objects.nonNull(result)) {
            response.getWriter().print(result);
        }
    }

}
