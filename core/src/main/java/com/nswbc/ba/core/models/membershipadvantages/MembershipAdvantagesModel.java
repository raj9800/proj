package com.nswbc.ba.core.models.membershipadvantages;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.nswbc.ba.core.models.membershipadvantages.columns.MembershipAdvantagesColumnModel;

/**
 * Membership component sling model.
 *
 * @author rajat.pachouri
 */
@Model(adaptables = SlingHttpServletRequest.class, resourceType = "nswbc-ba/components/content/membership-advantages",
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = "jackson", extensions = {"json"})
public class MembershipAdvantagesModel {

    /**
     * heading.
     */
    @Inject
    @Via("resource")
    private String heading;

    /**
     * color of the text.
     */
    @Inject
    @Via("resource")
    private String textColor;

    /**
     * getter for heading.
     *
     * @return heading
     */
    public String getHeading() {
        return heading;
    }

    /**
     * getter for textColor.
     *
     * @return textColor
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * columns.
     */
    @Inject
    @Via("resource")
    private List<MembershipAdvantagesColumnModel> columns;

    /**
     * Getter for columns.
     *
     * @return columns
     */
    public List<MembershipAdvantagesColumnModel> getColumns() {
        return columns;
    }
}
