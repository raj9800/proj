package com.nswbc.ba.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface ActiveTokenCheckService {
    /**
     * @param request
     * @return abn check
     */
    JsonObject verifyToken(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
