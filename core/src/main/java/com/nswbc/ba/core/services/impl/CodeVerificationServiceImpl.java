package com.nswbc.ba.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.CodeVerificationService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;
import com.nswbc.commons.core.utils.ServerSideValidationUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = CodeVerificationService.class)
public class CodeVerificationServiceImpl implements CodeVerificationService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(CodeVerificationServiceImpl.class);
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** private reference variable for my account configuration. */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;

    /**
     * returns the status of the security code.
     */
    @Override
    public JsonObject verifySecurityCode(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        JsonObject bodyJson = RestUtil.getBodyJson(request);
        boolean validate = verifySecurityCode(bodyJson.get("code").getAsString());
        if (validate && !bodyJson.isJsonNull()) {
            Map<String, String> headerMap = setHeadersMap();
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse restResponse = RestUtil.processRestPostRequest(client, registerConfig.getConfig().codeVerificationAPI(), headerMap,
                    bodyJson);
            if (Objects.nonNull(restResponse) && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                    RestUtil.closeHttpClient(client);
                    return RestUtil.getServerErrorResponse(response);
                } else {
                    try {
                        String responseEntity = EntityUtils.toString(restResponse.getEntity());
                        JsonObject securityCodeResult = new JsonParser().parse(responseEntity).getAsJsonObject();
                        securityCodeResult.addProperty(STATUS, Constants.Number.ONE);
                        return securityCodeResult;
                    } catch (ParseException | IOException e) {
                        LOG.error("Unable to register : ", e.getMessage());
                        return RestUtil.getServerErrorResponse(response);
                    } finally {
                        RestUtil.closeHttpClient(client);
                    }
                }
            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @param securityCode
     * @return secury code verification status
     */
    private boolean verifySecurityCode(String securityCode) {
        boolean validate = false;
        if (Objects.nonNull(securityCode) && StringUtils.isNotBlank(securityCode)) {
            validate = ServerSideValidationUtil.verifySecurityCode(securityCode);
        }
        return validate;
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, registerConfig.getConfig().subscriptionKey());
        return headerMap;
    }

}
