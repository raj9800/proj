package com.nswbc.ba.core.services;

import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author rajat.pachouri
 *
 */
public interface AgentAssistedRegistrationService {
    /**
     * @param request
     * @return json response in registration call
     * @throws IOException    IOException
     * @throws ParseException ParseException
     */
    JsonObject register(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ParseException, IOException;
}
