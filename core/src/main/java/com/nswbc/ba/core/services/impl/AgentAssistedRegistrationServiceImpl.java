package com.nswbc.ba.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.AbnGovLookupService;
import com.nswbc.ba.core.services.AgentAssistedRegistrationService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = AgentAssistedRegistrationService.class)
public class AgentAssistedRegistrationServiceImpl implements AgentAssistedRegistrationService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(AgentAssistedRegistrationServiceImpl.class);
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant for message. */
    private static final String MESSAGE = "message";
    /** Constant for USER_EXISTS_MESSAGE. */
    private static final String USER_EXISTS_MESSAGE = "User already registered!";

    /**
     * private reference for registration configuration.
     */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;

    /** private reference variable for abn gov verification. */
    @Reference
    private AbnGovLookupService abnGovVerify;

    /**
     * This method returns the response json object in registration call.
     *
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public JsonObject register(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ParseException, IOException {
        JsonObject bodyJson = RestUtil.getBodyJson(request);
        boolean validate = (Objects.nonNull(bodyJson) ? verifyInputFields(bodyJson) : false);
        if (validate) {
            Map<String, String> headerMap = setHeadersMap();
            CloseableHttpClient client = HttpClientBuilder.create().build();
            HttpResponse restResponse = RestUtil.processRestPostRequest(client,
                    registerConfig.getConfig().agentAssistedRegistrationAPI(), headerMap, bodyJson);
            if (Objects.nonNull(restResponse)
                    && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED)) {
                if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED) {
                    String responseEntity = EntityUtils.toString(restResponse.getEntity());
                    JsonObject registrationAPIResult = new JsonParser().parse(responseEntity).getAsJsonObject();
                    if (registrationAPIResult.has(MESSAGE)) {
                        if (registrationAPIResult.get(MESSAGE).getAsString().equals(USER_EXISTS_MESSAGE)) {
                            JsonObject registrationFinalResult = new JsonObject();
                            registrationFinalResult.addProperty(MESSAGE,
                                    "This email already exists within our system. please try again with a different email.");
                            response.setStatus(Constants.Number.FOUR_HUNDRED);
                            return registrationFinalResult;
                        }
                    }
                    return RestUtil.getServerErrorResponse(response);
                }
                try {
                    if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
                        JsonObject registrationFinalResult = new JsonObject();
                        registrationFinalResult.addProperty(STATUS, Constants.Number.ONE);
                        return registrationFinalResult;
                    } else {
                        RestUtil.getServerErrorResponse(response);
                    }
                } catch (ParseException e) {
                    LOG.error("Unable to register : ", e.getMessage());
                    return RestUtil.getServerErrorResponse(response);
                } finally {
                    RestUtil.closeHttpClient(client);
                }

            }
        }
        return RestUtil.getBadRequestResponse(response);
    }

    /**
     * @param bodyJson This method is use to verify input form fields
     * @return validation status for fields
     */
    private boolean verifyInputFields(JsonObject bodyJson) {
        boolean validate = false;
        boolean validateFirstName = false;
        boolean validateLastName = false;
        boolean validateEmail = false;
        String firstName = bodyJson.get("firstname").toString();
        String lastName = bodyJson.get("lastname").toString();
        String email = bodyJson.get("email").toString();
        if (Objects.nonNull(firstName) && StringUtils.isNotBlank(firstName)) {
            validateFirstName = true;
        }
        if (Objects.nonNull(lastName) && StringUtils.isNotBlank(lastName)) {
            validateLastName = true;
        }
        if (Objects.nonNull(email) && StringUtils.isNotBlank(email)) {
            validateEmail = true;
        }
        if (validateFirstName && validateLastName && validateEmail) {
            validate = true;
            return validate;
        }
        return validate;
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, registerConfig.getConfig().agentAssistedRegistrationSubscriptionKey());
        return headerMap;
    }
}
