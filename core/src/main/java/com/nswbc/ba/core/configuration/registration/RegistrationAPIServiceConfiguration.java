package com.nswbc.ba.core.configuration.registration;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

/**
 * @author yuvraj.bansal Interface for configuration
 *
 */
@ObjectClassDefinition(name = "Registration API Configurations", description = "Service Configuration")
public @interface RegistrationAPIServiceConfiguration {
    /**
     * @return emailCheckAPI
     */
    @AttributeDefinition(name = "Email Check API", description = "Provide the api url for email check.")
    String emailCheckAPI();

    /**
     * @return abnCheckAPI
     */
    @AttributeDefinition(name = "ABN Check API", description = "Provide the api url for abn check.")
    String abnCheckAPI();

    /**
     * @return registrationAPI
     */
    @AttributeDefinition(name = "Registration API", description = "Provide the api url for registration.")
    String registrationAPI();

    /**
     * @return agentAssistedRegistrationAPI
     */
    @AttributeDefinition(name = "Agent Assisted Registration API",
            description = "Provide the api url for agent assisted registration.")
    String agentAssistedRegistrationAPI();

    /**
     * @return agentAssistedRegistrationSubscriptionKey
     */
    @AttributeDefinition(name = "Agent Assited Registration Subscription Key",
            description = "Provide the subscription key.")
    String agentAssistedRegistrationSubscriptionKey();

    /**
     * @return abnGovAPI
     */
    @AttributeDefinition(name = "ABN Gov API", description = "Provide the api url for abn check.")
    String abnGovAPI();

    /**
     * @return abnGovAPIGUID
     */
    @AttributeDefinition(name = "GUID to check abn with gov", description = "Provide the guid.")
    String abnGovAPIGUID();

    /**
     * @return abnGovAPIGUID
     */
    @AttributeDefinition(name = "Activate Member Check API",
            description = "Provide the api url to check token validity.")
    String tokenVerificationAPI();

    /**
     * @return registrationAPI
     */
    @AttributeDefinition(name = "Subscription Key", description = "Provide the subscription key.")
    String subscriptionKey();

    /**
     * @return codeVerificationAPI
     */
    @AttributeDefinition(name = "Code Verification API", description = "Provide the code verification api details.")
    String codeVerificationAPI();
}
