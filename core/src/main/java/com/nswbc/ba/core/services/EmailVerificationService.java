package com.nswbc.ba.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface EmailVerificationService {
    /**
     * @param request
     * @param response
     * @return verify email status
     */
    JsonObject verifyEmail(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
