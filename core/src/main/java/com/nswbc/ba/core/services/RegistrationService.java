package com.nswbc.ba.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface RegistrationService {
    /**
     * @param request
     * @return json response in registration call
     */
    JsonObject register(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
