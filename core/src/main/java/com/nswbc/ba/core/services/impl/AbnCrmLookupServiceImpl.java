package com.nswbc.ba.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.AbnCrmLookupService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = AbnCrmLookupService.class)
public class AbnCrmLookupServiceImpl implements AbnCrmLookupService {
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(AbnCrmLookupServiceImpl.class);
    /** Constant for abn. */
    private static final String ABN = "abn";
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for status. */
    private static final String STATUS = "status";
    /**
     * private reference for registration configuration.
     */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;

    /**
     * This method checks the abn from crm api and returns the status response.
     */
    @Override
    public JsonObject verifyABN(String abn, SlingHttpServletResponse response) {
        Map<String, String> headerMap = setHeadersMap();
        JsonObject json = new JsonObject();
        json.addProperty(ABN, abn);
        CloseableHttpClient client = HttpClientBuilder.create().build();
        HttpResponse restResponse = RestUtil.processRestPostRequest(client, registerConfig.getConfig().abnCheckAPI(), headerMap, json);
        if (Objects.nonNull(restResponse) && !(restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED)) {
            if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
                RestUtil.closeHttpClient(client);
                return getServerErrorResponse();
            }
            try {
                String responseEntity = EntityUtils.toString(restResponse.getEntity());
                return new JsonParser().parse(responseEntity).getAsJsonObject();
            } catch (ParseException | IOException e) {
                LOG.error("Unable to get abn status : ", e.getMessage());
                return getServerErrorResponse();
            } finally {
                RestUtil.closeHttpClient(client);
            }
        }
        return getBadRequestResponse();

    }

    /**
     * @param response
     * @return 400 server error
     */
    private JsonObject getBadRequestResponse() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(STATUS, Constants.Number.FOUR_HUNDRED);
        return jsonObject;
    }

    /**
     * @param response
     * @return 500 server error
     */
    private JsonObject getServerErrorResponse() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(STATUS, Constants.Number.FIVE_HUNDRED);
        return jsonObject;
    }
    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, registerConfig.getConfig().subscriptionKey());
        return headerMap;
    }

}
