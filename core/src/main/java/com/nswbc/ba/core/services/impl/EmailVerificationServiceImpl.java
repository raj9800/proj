package com.nswbc.ba.core.services.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nswbc.ba.core.configuration.registration.RegistrationAPIServiceConfigurationInstance;
import com.nswbc.ba.core.services.EmailVerificationService;
import com.nswbc.commons.core.constants.Constants;
import com.nswbc.commons.core.utils.RestUtil;

/**
 * @author yuvraj.bansal
 *
 */
@Component(immediate = true, service = EmailVerificationService.class)
public class EmailVerificationServiceImpl implements EmailVerificationService {
    /** Constant for Subscription key. */
    private static final String SUBSCRIPTION_KEY = "Ocp-Apim-Subscription-Key";
    /** Constant for email. */
    private static final String EMAIL = "email";
    /** Constant for status. */
    private static final String STATUS = "status";
    /** Constant for Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(RestUtil.class);
    /**
     * private reference for registration configuration.
     */
    @Reference
    private RegistrationAPIServiceConfigurationInstance registerConfig;

    /**
     * This method checks the mail from api and returns the status response.
     */
    @Override
    public JsonObject verifyEmail(SlingHttpServletRequest request, SlingHttpServletResponse response) {
        String email = request.getParameter("email");
        boolean validate = verifyInputField(email);
        if (validate) {
            JsonObject result = emailCheckProcessing(request, registerConfig.getConfig().emailCheckAPI(),
                    registerConfig.getConfig().subscriptionKey(), response, email);
            if (Objects.nonNull(result)) {
                return result;
            }
        }
        return RestUtil.getBadRequestResponse(response);

    }

    /**
     * @param email This method is use to verify input email field.
     * @return validation status of email field
     */
    private boolean verifyInputField(String email) {
        boolean validate = false;
        if (Objects.nonNull(email) && StringUtils.isNotBlank(email)) {
            validate = true;
        }
        return validate;
    }

    /**
     * @param request
     * @param api
     * @param subscriptionKey
     * @param email
     * @return JsonElement This method returns the output from the email check api
     *         in the form of json.
     */
    private JsonObject emailCheckProcessing(SlingHttpServletRequest request, String api, String subscriptionKey,
            SlingHttpServletResponse response, String email) {
        JsonObject resultElement = new JsonObject();
        Map<String, String> headerMap = setHeadersMap();
        CloseableHttpClient client = HttpClientBuilder.create().build();
        JsonObject json = new JsonObject();
        json.addProperty(EMAIL, email);
        HttpResponse restResponse = RestUtil.processRestPostRequest(client, api, headerMap, json);
        if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED) {
            // existing
            resultElement = setUserStatus(client, resultElement, restResponse);
        } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.TWO_HUNDRED_FOUR) {
            // never existed, brand new
            resultElement.addProperty(STATUS, Constants.Number.ZERO);
        } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FOUR_HUNDRED) {
            return RestUtil.getBadRequestResponse(response);
        } else if (restResponse.getStatusLine().getStatusCode() == Constants.Number.FIVE_HUNDRED) {
            return RestUtil.getServerErrorResponse(response);
        }
        return resultElement;
    }

    /**
     * @return header map in request.
     */
    private Map<String, String> setHeadersMap() {
        Map<String, String> headerMap = new HashMap<String, String>();
        headerMap.put(Constants.Application.CONTENT_TYPE, Constants.Application.APPLICATION_JSON);
        headerMap.put(SUBSCRIPTION_KEY, registerConfig.getConfig().subscriptionKey());
        return headerMap;
    }

    /**
     * @param client
     * @param resultElement
     * @param restResponse
     * @throws IOException This method set the status of existing users or new
     *                     users, which have registered but not verified themselves.
     * @return json object
     */
    private JsonObject setUserStatus(CloseableHttpClient client, JsonObject resultElement, HttpResponse restResponse) {
        String result;
        try {
            result = EntityUtils.toString(restResponse.getEntity());
            JsonObject user = new JsonParser().parse(result).getAsJsonObject();
            Boolean isVerified = user.get("isVerified").getAsBoolean();
            Boolean isMigrated = user.get("isMigrated").getAsBoolean();
            Boolean isEnabled = user.get("isEnabled").getAsBoolean();
            Boolean forceChangePass = user.get("forceChangePasswordNextLogin").getAsBoolean();
            if (isEnabled) {
                if (!isVerified && !isMigrated) {
                    resultElement.addProperty(STATUS, Constants.Number.ZERO); // new
                } else if (isMigrated && forceChangePass) {
                    resultElement.addProperty(STATUS, Constants.Number.TWO); // password change
                } else {
                    resultElement.addProperty(STATUS, Constants.Number.ONE); // existing
                }
            } else {
                resultElement.addProperty(STATUS, Constants.Number.ZERO); // new
            }
            client.close();
        } catch (ParseException | IOException e) {
            LOG.error("Unable to set existing user status in email processing: ", e.getMessage());
        }
        return resultElement;
    }

}
