package com.nswbc.ba.core.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;

import com.google.gson.JsonObject;

/**
 * @author yuvraj.bansal
 *
 */
public interface AbnGovLookupService {
    /**
     * @param request
     * @return abn check
     */
    JsonObject verifyABNGov(SlingHttpServletRequest request, SlingHttpServletResponse response);
}
