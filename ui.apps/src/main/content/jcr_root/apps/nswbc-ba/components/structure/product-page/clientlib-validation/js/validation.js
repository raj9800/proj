/** a JS file that shall be included */

(function($) {

    var REGEX_SELECTOR = "regex.validation.guid",
    foundationReg = $(window).adaptTo("foundation-registry");

    foundationReg.register("foundation.validation.validator", {
        selector: "[data-validation='" + REGEX_SELECTOR + "']",
        validate: function(el) {

            var error_message = "Code already used";
            var guid = el.value;
            var codeUsed = true;
            var currentUrl = new URL(window.location.href);
            var path = currentUrl.searchParams.get("item");
            jQuery.ajax({
              url: '/bin/nswbc/ba/productauthor.guidcheck.json',
              data: {"guid" : guid, "path" : path},
              success: function (data) {
                codeUsed = false;
              },
               async: false
           });

           if(codeUsed){
            return error_message;
           }
        }
    });

}(jQuery));