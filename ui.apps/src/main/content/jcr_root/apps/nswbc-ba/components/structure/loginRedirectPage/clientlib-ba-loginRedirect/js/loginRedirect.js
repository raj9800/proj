// Login() variables
var envHostname, apiHostname, clientID, authorityID;
var appConfig = {};
var msalConfig = {};
var msalConfigPR = {};

// Login() variables
// -----------------------------------------------------------
// Login JS on page load
// -----------------------------------------------------------
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    var data = JSON.parse(this.response);
    envHostname = data.envHostname;
    apiHostname = data.apiHostname;
    clientID = data.clientID
    authorityID = data.authorityID;
    allowedRedirectDomains = data.allowedRedirectDomains;
  }
};
xhttp.open("GET", "/bin/nswbc/loginclient.ba.json", false);
xhttp.send();
appConfig = {
  b2cScopes: ["https://" + envHostname + ".onmicrosoft.com/integration/api"],
  webApi: "https://" + apiHostname + ".nswbc.com.au/api/member/v4/profile"
};
// configuration to initialize msal for login
msalConfig = {
  auth: {
    clientId: clientID, // This is your client ID
    authority: "https://" + envHostname + ".b2clogin.com/" + authorityID + "/B2C_1_SI",
    validateAuthority: false
  },
  cache: {
    cacheLocation: "localStorage",
    storeAuthStateInCookie: true
  }

};
//configuration to initialize msal for forgot pwd
msalConfigPR = {
    auth: {
        clientId: clientID, // This is your client ID
        authority: "https://" + envHostname + ".b2clogin.com/" + authorityID + "/B2C_1_FP", 
        validateAuthority:false
    },
    cache: {
        cacheLocation: "localStorage",
        storeAuthStateInCookie: true
    }
    
};
// function to get query params
function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,i;
    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  }
//call back function which will be triggered when the request comes from login or forgot password screen
function onAuthCallback(errorDesc, token, error, tokenType) {
  errorDesc = errorDesc + '';
}
//get a cookie
function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
//updating value of cookie to redirect to home page
//document.cookie = "msal.login.request=" + getCookie('request.url');

//defining global client app object 
var clientApplication = new Msal.UserAgentApplication(msalConfig);
// if a user is logged i, populate access token
if (clientApplication.getAccount() !== null) {
	populateAccessToken();
} else {
//	Register a call back for redirect flow
	clientApplication.handleRedirectCallback(onAuthCallback);
}

//login function
function loginMsal(email) {
  var loginRequest = {
    scopes: appConfig.b2cScopes
  };
  if (typeof email !== 'undefined') {
    loginRequest.extraQueryParameters = {
      email: email
    };
  }
  clientApplication.loginRedirect(loginRequest);
}
//function to get access token
function populateAccessToken() {
	var tokenRequest = {
			scopes: appConfig.b2cScopes
	}
	clientApplication.acquireTokenSilent(tokenRequest).then(function(tokenResponse) {
		var access_token = tokenResponse.accessToken;
		document.cookie = "access_token=" + access_token +";path=/";
	}).catch(function(error) {
		// fetching token from local storage is fetching was unsuccessfull
		var access_token_local = findValueByPrefix(localStorage, '{"authority');
		if (access_token_local !== null) {
			access_token_local = JSON.parse(access_token_local);
			access_token_local = access_token_local.accessToken;
			document.cookie = "access_token=" + access_token_local + ";path=/";
		} else {
			document.cookie = "access_token=" + ";path=/";
		}
	}).finally(function(error) {
		// redirecting after the token is generated.
		location.href = getCookie('request.url');
	});
}

function findValueByPrefix(object, prefix) {
  for (var property in object) {
    if (object.hasOwnProperty(property) && property.toString().startsWith(prefix)) {
      return object[property];
    }
  }
};
//function to forgot password
function forgotPassword(email) {
    var loginRequestPR = {
        scopes: appConfig.b2cScopes
    };
    if (typeof email !== 'undefined'){
        loginRequestPR.extraQueryParameters = {email:email}
    }
    var clientApplicationPR = new Msal.UserAgentApplication(msalConfigPR);
    // Register a call back for redirect flow
    clientApplicationPR.handleRedirectCallback(onAuthCallback);
    clientApplicationPR.loginRedirect(loginRequestPR);
  }
//redirect the user to the home page if there is no referrer
function checkReferrer(){
	if(document.referrer === '' || document.referrer === undefined) {
            location.href = location.protocol+"//"+location.hostname;
        }
}
//call login function
if (clientApplication.getAccount() === null) {
	checkReferrer();
	document.getElementById('post-login').style.display="none";
	document.getElementById('pre-login').style.display="block";
	if(getUrlParameter('post_login_redirect') === undefined){
		//updating value of cookie to redirect to the page from where the user has come
		document.cookie = "request.url=" + document.referrer;
	}else {
    	var domain_name = getUrlParameter('post_login_redirect').replace('http://','').replace('https://','').split(/[/?#]/)[0];
    	var isAllowedDomain = false;
    	for (i=0; i < allowedRedirectDomains.length; i++) {
            if (domain_name === allowedRedirectDomains[i]) {
                isAllowedDomain = true;
                break;
            }
    	}
    	if (isAllowedDomain) {
    		//updating value of cookie to redirect to the page based on the query param
            document.cookie = "request.url=" + getUrlParameter('post_login_redirect');
    	}else {
    		document.cookie = "request.url=" + location.protocol + '//' + location.hostname;
    	}
    }
	var prefill_email = getUrlParameter('prefill_email');
	//send the user to reset password screen based upon origin parameter
	if(getUrlParameter('origin_flow') === 'resetpassword' && prefill_email){
		forgotPassword(prefill_email);
	}//send the user to login screen based upon email parameter
	else if(prefill_email === undefined){
		loginMsal();
	}else {
		loginMsal(getUrlParameter('prefill_email'));
	}
}else{
	document.getElementById('pre-login').style.display="none";
	document.getElementById('post-login').style.display="block";
}

