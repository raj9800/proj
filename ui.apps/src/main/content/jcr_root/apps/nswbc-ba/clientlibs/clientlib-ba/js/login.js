document.addEventListener("DOMContentLoaded", function () {
    if (clientApplication.getAccount() && clientApplication.getAccount().idToken) {
        //check token expiry
        if((clientApplication.getAccount().idToken.aud === clientID) && (Math.round((new Date()).getTime() / 1000) < clientApplication.getAccount().idToken.exp)){
            updateUI();
        }else{
            clientApplication.logout();
        }
    }
});

//update aem ui
function updateUI(){
    var firstName = clientApplication.getAccount().idToken.given_name || "";
    $(".bc-header-topbar__list").addClass("loggedin");
    $(".first-name").html(firstName)
    $(".login-nv-mobile").hide();
    $(".login-nav").hide();
    $(".join-for-free").hide();
    $(".bc-nav-loggedin-dropdown").show();
}

